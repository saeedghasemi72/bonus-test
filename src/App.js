import React, { useEffect } from "react";
import "./App.css";

import Layout from "./components/Layout";
import TodoListCategory from "./components/TodoListCategory";
import AddTodoList from "./components/AddTodoList";
import { useDispatch, useSelector } from "react-redux";
import { sendInitTodos } from "./store/actions/getInitTodos";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { dragDrop } from "./store/actions/dragDrop";
import convertDate from "./helpers/date";
import { showSnackbar } from "./store/actions/showSnackbar";

function App() {
  const Dispatch = useDispatch();
  const todoListCategory = useSelector((state) => state.todoLists);
  const date = new Date();

  const fetchData = () => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((res) => res.json())
      .then((res) => {
        Dispatch(
          sendInitTodos({
            title: "Init Todos",
            date: `${date.getFullYear()}-${date.getMonth()}-${date.getDay()} ${date.getHours()}:${date.getMinutes()}`,
            todos: res.slice(0, 5),
          })
        );
        Dispatch(
          showSnackbar({
            message: "Get success Init TodoList ",
            type: "success",
          })
        );
      })
      .catch((err) => {
        Dispatch(
          showSnackbar({
            message: "Error Get Init TodoList ",
            type: "error",
          })
        );
      });
  };

  useEffect(() => {
    todoListCategory.length < 1 && fetchData();
  });

  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = JSON.parse(JSON.stringify(source));
    const destClone = JSON.parse(JSON.stringify(destination));
    const [removed] = sourceClone.todos.splice(droppableSource.index, 1);
    destClone.todos.splice(droppableDestination.index, 0, removed);
    const result = [];
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
  };

  const onDragEnd = (result) => {
    const { source, destination } = result;
    Dispatch(
      showSnackbar({
        message: "The move was successful",
        type: "success",
      })
    );
    if (!destination) {
      return;
    }
    const sInd = +source.droppableId;
    const dInd = +destination.droppableId;

    if (sInd === dInd) {
      const items = reorder(
        todoListCategory[sInd].todos,
        source.index,
        destination.index
      );
      const newState = JSON.parse(JSON.stringify(todoListCategory));
      newState[sInd].todos = items;
      newState[sInd].updated = convertDate(new Date());
      Dispatch(dragDrop(newState));
    } else {
      const result = move(
        todoListCategory[sInd],
        todoListCategory[dInd],
        source,
        destination
      );
      const newState = JSON.parse(JSON.stringify(todoListCategory));
      newState[sInd].todos = result[sInd].todos;
      newState[sInd].updated = convertDate(new Date());
      newState[dInd].todos = result[dInd].todos;
      newState[dInd].updated = convertDate(new Date());
      Dispatch(dragDrop(newState));
    }
  };

  return (
    <div className="App">
      <Layout>
        <DragDropContext onDragEnd={onDragEnd}>
          {todoListCategory.map((category, index) => (
            <Droppable key={index} droppableId={`${index}`}>
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  // style={getListStyle(snapshot.isDraggingOver)}
                  {...provided.droppableProps}
                >
                  <TodoListCategory
                    categoryIndex={index}
                    title={category.title}
                    todosList={category.todos}
                    date={category.date}
                    updated={category.updated}
                    key={`${category}-${index}`}
                  />
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          ))}
        </DragDropContext>
        <AddTodoList />
      </Layout>
    </div>
  );
}

export default App;
