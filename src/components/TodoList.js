import React, { memo } from "react";
import { List, Paper, Grid } from "@material-ui/core";
import TodoListItem from "./TodoListItem";
import { Draggable } from "react-beautiful-dnd";

const TodoList = memo((props) => (
  <>
    {props.items.length > 0 && (
      <Paper style={{ margin: 16 }}>
        <List style={{ overflow: "scroll" }}>
          {/* {props.items.map((todo, idx) => (
            <TodoListItem
              {...todo}
              idx={idx}
              key={`TodoItem.${idx}`}
              categoryIndex={props.categoryIndex}
              divider={idx !== props.items.length - 1}
              onButtonClick={() => props.onItemRemove(idx, props.categoryIndex)}
              onCheckBoxToggle={() => props.onItemCheck(idx)}
              onEditClick
            />
          ))} */}

          {props.items.map((todo, idx) => (
            <Draggable
              key={todo.id}
              draggableId={`${todo.title}-${idx}`}
              index={idx}
            >
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                  // style={getItemStyle(
                  //   snapshot.isDragging,
                  //   provided.draggableProps.style
                  // )}
                >
                  <div>
                    <TodoListItem
                      {...todo}
                      idx={idx}
                      key={`TodoItem.${idx}`}
                      categoryIndex={props.categoryIndex}
                      divider={idx !== props.items.length - 1}
                      onButtonClick={() =>
                        props.onItemRemove(idx, props.categoryIndex)
                      }
                      onCheckBoxToggle={() => props.onItemCheck(idx)}
                      onEditClick
                    />
                  </div>
                </div>
              )}
            </Draggable>
          ))}
        </List>
      </Paper>
    )}
  </>
));
export default TodoList;
