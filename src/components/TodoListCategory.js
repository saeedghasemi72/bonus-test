import React, { useState } from "react";
import { useInputValue, useTodos } from "./CustomHooks";
import AddTodo from "./AddTodo";
import TodoList from "./TodoList";
import {
  Grid,
  Typography,
  IconButton,
  ListItem,
  DialogTitle,
  List,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import SettingsIcon from "@material-ui/icons/Settings";
import Dialog from "@material-ui/core/Dialog";
import CloseIcon from "@material-ui/icons/Close";
import { useDispatch } from "react-redux";
import { deleteTodoList } from "../store/actions/deleteTodoList";

const useStyles = makeStyles((theme) => ({
  grid: {
    flexWrap: "nowrap",
    minWidth: "500px",
  },
}));

const TodoListCategory = ({
  todosList,
  title,
  categoryIndex,
  date,
  updated,
}) => {
  const Dispatch = useDispatch();
  const [showDialog, setShowDialog] = useState(false);
  const classes = useStyles();
  const { inputValue, changeInput, clearInput, keyInput } = useInputValue();
  const { addTodo, checkTodo, removeTodo } = useTodos();
  const clearInputAndAddTodo = () => {
    clearInput();
    addTodo(inputValue, categoryIndex);
  };

  const HandelDelete = () => {
    setShowDialog(!showDialog);
    Dispatch(deleteTodoList(categoryIndex));
  };

  return (
    <Grid
      item
      className={classes.grid}
      style={{
        margin: 10,
        backgroundColor: "#eee",
        height: "fit-content",
        padding: 10,
      }}
    >
      <ListItem style={{ justifyContent: "space-between" }}>
        <Typography color="inherit">{title}</Typography>
        <IconButton
          aria-label="Show Dialog"
          onClick={() => setShowDialog(!showDialog)}
        >
          <SettingsIcon />
        </IconButton>
      </ListItem>

      <AddTodo
        inputValue={inputValue}
        onInputChange={changeInput}
        onButtonClick={clearInputAndAddTodo}
        onInputKeyPress={(event) => keyInput(event, clearInputAndAddTodo)}
      />
      {todosList && (
        <TodoList
          categoryIndex={categoryIndex}
          items={todosList}
          onItemCheck={(idx) => checkTodo(idx)}
          onItemRemove={(idx, categoryIndex) => removeTodo(idx, categoryIndex)}
        />
      )}
      <Dialog
        // onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={showDialog}
      >
        <DialogTitle id="simple-dialog-title">
          <ListItem style={{ justifyContent: "space-between" }}>
            <Typography color="inherit">{title}</Typography>
            <IconButton aria-label="Show Dialog">
              <CloseIcon onClick={() => setShowDialog(!showDialog)} />
            </IconButton>
          </ListItem>
        </DialogTitle>
        <List>
          <ListItem>Create Date : {date}</ListItem>
          <ListItem>Update Date : {updated ? updated : date}</ListItem>

          <Button
            fullWidth
            color="secondary"
            variant="outlined"
            onClick={HandelDelete}
          >
            Delete TodoList
          </Button>
        </List>
      </Dialog>
    </Grid>
  );
};

export default TodoListCategory;
