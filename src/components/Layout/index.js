import React from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Paper,
  Grid,
  Snackbar,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Alert from "@material-ui/lab/Alert";
import { useSelector, useDispatch } from "react-redux";
import { hideSnackbar } from "../../store/actions/hideSnackbar";

const useStyles = makeStyles((theme) => ({
  grid: {
    flexWrap: "nowrap",
  },
}));

const Layout = (props) => {
  const Dispatch = useDispatch();
  const classes = useStyles();
  const snackBar = useSelector((state) => state.showSnackbar);
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    Dispatch(hideSnackbar());
  };
  return (
    <Paper
      elevation={0}
      style={{ padding: 0, margin: 20, backgroundColor: "unset" }}
    >
      <AppBar color="primary" position="fixed" style={{ height: 64 }}>
        <Toolbar style={{ height: 64 }}>
          <Typography color="inherit">
            TODO APP - Develop by{" "}
            <a
              href="https://www.linkedin.com/in/saeed-ghasemi94/"
              style={{ color: "white" }}
            >
              Saeed Ghasemi
            </a>
          </Typography>
        </Toolbar>
      </AppBar>
      <Grid
        container
        spacing={3}
        className={classes.grid}
        style={{ marginTop: 100 }}
      >
        {props.children}
        <Snackbar
          open={snackBar.show}
          autoHideDuration={2000}
          onClose={handleClose}
        >
          <Alert severity={snackBar.type}>{snackBar.message}</Alert>
        </Snackbar>
      </Grid>
    </Paper>
  );
};
export default Layout;
