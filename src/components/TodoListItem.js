import React, { memo, useState } from "react";
import {
  List,
  ListItem,
  Checkbox,
  IconButton,
  ListItemText,
  ListItemSecondaryAction,
  TextField,
} from "@material-ui/core";
import DeleteOutlined from "@material-ui/icons/DeleteOutlined";
import { useInputValue } from "./CustomHooks";
import EditIcon from "@material-ui/icons/Edit";
import CloseIcon from "@material-ui/icons/Close";
import SendIcon from "@material-ui/icons/Send";
import { useDispatch } from "react-redux";
import { editTodo } from "../store/actions/editTodo";
import convertDate from "../helpers/date";
import { showSnackbar } from "../store/actions/showSnackbar";

const TodoListItem = ({
  categoryIndex,
  title,
  onCheckBoxToggle,
  onInputKeyPress,
  complated,
  divider,
  onButtonClick,
  idx,
}) => {
  const Dispatch = useDispatch();

  const { inputValue, changeInput, keyInput } = useInputValue(title);
  const [showEdit, setShowEdit] = useState(false);

  const handelEditTodo = () => {
    Dispatch(
      editTodo({
        idx,
        categoryIndex,
        title: inputValue,
        update: convertDate(new Date()),
      })
    );
    Dispatch(
      showSnackbar({
        message: "success edit TODO",
        type: "success",
      })
    );
    setShowEdit(!showEdit);
  };

  return (
    <ListItem divider={divider}>
      <Checkbox onClick={onCheckBoxToggle} checked={complated} disableRipple />
      {showEdit ? (
        <TextField
          placeholder="Add Todo here"
          value={inputValue}
          onChange={changeInput}
          onKeyPress={(event) => keyInput(event, handelEditTodo)}
          fullWidth
        />
      ) : (
        <ListItemText primary={title} />
      )}

      <ListItemSecondaryAction>
        {!showEdit && (
          <IconButton
            aria-label="Edit Todo"
            onClick={() => setShowEdit(!showEdit)}
          >
            <EditIcon />
          </IconButton>
        )}

        {!showEdit && (
          <IconButton aria-label="Delete Todo" onClick={onButtonClick}>
            <DeleteOutlined />
          </IconButton>
        )}
        {showEdit && (
          <IconButton aria-label="Send Todo" onClick={handelEditTodo}>
            <SendIcon />
          </IconButton>
        )}
        {showEdit && (
          <IconButton aria-label="Close" onClick={() => setShowEdit(!showEdit)}>
            <CloseIcon />
          </IconButton>
        )}
      </ListItemSecondaryAction>
    </ListItem>
  );
};
export default memo(TodoListItem);
