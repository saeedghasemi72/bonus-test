import { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "../../store/actions/addTodo";
import { deleteTodo } from "../../store/actions/deleteTodo";
import convertDate from "../../helpers/date";
import { showSnackbar } from "../../store/actions/showSnackbar";

export const useTodos = (initialValue = []) => {
  const Dispatch = useDispatch();
  const [todos, setTodos] = useState(initialValue);
  return {
    todos,
    addTodo: (title, categoryIndex) => {
      if (title !== "") {
        Dispatch(
          addTodo({ categoryIndex, title, update: convertDate(new Date()) })
        );
        Dispatch(
          showSnackbar({
            message: "Add New Todo",
            type: "info",
          })
        );
      }
    },
    checkTodo: (idx) => {
      setTodos(
        todos.map((todo, index) => {
          if (idx === index) {
            todo.checked = !todo.checked;
          }
          return todo;
        })
      );
    },
    removeTodo: (idx, categoryIndex) => {
      Dispatch(
        showSnackbar({
          message: "Remove Todo",
          type: "info",
        })
      );
      Dispatch(
        deleteTodo({ idx, categoryIndex, update: convertDate(new Date()) })
      );
    },
  };
};

export const useInputValue = (initialValue = "") => {
  const [inputValue, setInputValue] = useState(initialValue);
  return {
    inputValue,
    changeInput: (event) => setInputValue(event.target.value),
    clearInput: () => setInputValue(""),
    keyInput: (event, callback) => {
      if (event.which === 13 || event.keyCode === 13) {
        callback(inputValue);
        return true;
      }
      return false;
    },
  };
};
