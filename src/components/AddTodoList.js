import React, { useState, memo } from "react";
import { TextField, Button, Grid, Icon } from "@material-ui/core";
import { Add, Close } from "@material-ui/icons";
import { useInputValue } from "./CustomHooks";
import { useDispatch } from "react-redux";
import { addTodoList } from "../store/actions/addTodoList";
import convertDate from "../helpers/date";
import { showSnackbar } from "../store/actions/showSnackbar";

const AddTodoList = () => {
  const Dispatch = useDispatch();
  const { inputValue, changeInput, clearInput } = useInputValue();
  // const date = new Date();
  // console.log(convertDate(new Date()));
  const [showForm, setShowForm] = useState(false);
  const handelShowForm = () => {
    setShowForm(!showForm);
  };
  const handelSubmit = (e) => {
    e.preventDefault();
    Dispatch(
      addTodoList({
        title: inputValue,
        todos: [],
        date: convertDate(new Date()),
      })
    );
    Dispatch(
      showSnackbar({
        message: "Add New TodoList",
        type: "info",
      })
    );
    clearInput();
    setShowForm(!showForm);
  };

  return (
    <Grid
      item
      // className={classes.grid}
      style={{
        margin: 10,
        padding: 15,
        backgroundColor: "#eee",
        height: "fit-content",
        minWidth: 200,
      }}
    >
      {showForm ? (
        <form
          //   className={classes.root}
          noValidate
          autoComplete="off"
          onSubmit={handelSubmit}
        >
          <TextField
            id="standard-basic"
            label="Enter List Title"
            onChange={changeInput}
            value={inputValue}
            style={{ display: "block", marginBottom: 20 }}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Button variant="contained" color="primary" type="submit">
              Add List
            </Button>
            <Close onClick={handelShowForm} style={{ cursor: "pointer" }} />
          </div>
        </form>
      ) : (
        <div
          style={{ cursor: "pointer", display: "flex" }}
          onClick={handelShowForm}
        >
          <Add />
          Add another list
        </div>
      )}
    </Grid>
  );
};

export default memo(AddTodoList);
