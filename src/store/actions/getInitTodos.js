import { INIT_TODOS } from "../types";

export const sendInitTodos = (payload) => {
  return {
    type: INIT_TODOS,
    payload,
  };
};
