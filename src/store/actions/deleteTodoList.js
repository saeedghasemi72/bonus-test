import { DELETE_TODOLIST } from "../types";

export const deleteTodoList = (payload) => {
  return {
    type: DELETE_TODOLIST,
    payload,
  };
};
