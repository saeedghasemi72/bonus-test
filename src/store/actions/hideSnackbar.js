import { HIDE_SNACKBAR } from "../types";

export const hideSnackbar = (payload) => {
  return {
    type: HIDE_SNACKBAR,
    payload,
  };
};
