import { EDIT_TODO } from "../types";

export const editTodo = (payload) => {
  return {
    type: EDIT_TODO,
    payload,
  };
};
