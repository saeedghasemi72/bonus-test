import { ADD_TODO } from "../types";

export const addTodo = (payload) => {
  return {
    type: ADD_TODO,
    payload,
  };
};
