import { DRAG_DROP } from "../types";

export const dragDrop = (payload) => {
  return {
    type: DRAG_DROP,
    payload,
  };
};
