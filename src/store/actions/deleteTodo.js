import { DELETE_TODO } from "../types";

export const deleteTodo = (payload) => {
  return {
    type: DELETE_TODO,
    payload,
  };
};
