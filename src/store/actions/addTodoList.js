import { ADD_TODO_LIST } from "../types";

export const addTodoList = (payload) => {
  return {
    type: ADD_TODO_LIST,
    payload,
  };
};
