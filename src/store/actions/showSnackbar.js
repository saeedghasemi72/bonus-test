import { SHOW_SNACKBAR } from "../types";

export const showSnackbar = (payload) => {
  return {
    type: SHOW_SNACKBAR,
    payload,
  };
};
