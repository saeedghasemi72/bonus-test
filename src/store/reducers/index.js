import {
  INIT_TODOS,
  ADD_TODO_LIST,
  ADD_TODO,
  DELETE_TODO,
  EDIT_TODO,
  DELETE_TODOLIST,
  DRAG_DROP,
  SHOW_SNACKBAR,
  HIDE_SNACKBAR,
} from "../types";

const initialState = {
  todoLists: [],
  showSnackbar: {
    show: false,
  },
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case INIT_TODOS:
      return {
        ...state,
        todoLists: [payload],
      };
    case ADD_TODO_LIST:
      return {
        todoLists: [...state.todoLists, payload],
      };
    case ADD_TODO:
      return {
        ...state,
        todoLists: state.todoLists.map((todoList, index) => {
          if (index === payload.categoryIndex) {
            return {
              ...todoList,
              updated: payload.update,
              todos: [
                ...todoList.todos,
                {
                  title: payload.title,
                  completed: false,
                },
              ],
            };
          }
          return todoList;
        }),
      };
    case DELETE_TODO:
      return {
        ...state,
        todoLists: state.todoLists.map((todoList, index) => {
          if (index === parseInt(payload.categoryIndex)) {
            return {
              ...todoList,
              updated: payload.update,
              todos: todoList.todos.filter((todo, index) => {
                console.log(index, payload.idx);
                return index !== payload.idx;
              }),
            };
          }
          return todoList;
        }),
      };
    case EDIT_TODO:
      return {
        ...state,
        todoLists: state.todoLists.map((todoList, index) => {
          if (index === payload.categoryIndex) {
            return {
              ...todoList,
              updated: payload.update,
              todos: todoList.todos.map((todo, index) => {
                if (index === payload.idx) {
                  return {
                    ...todo,
                    title: payload.title,
                  };
                }
                return todo;
              }),
            };
          }
          return todoList;
        }),
      };
    case DELETE_TODOLIST:
      return {
        ...state,
        todoLists: state.todoLists.filter(
          (todolist, index) => index !== payload
        ),
      };
    case DRAG_DROP:
      return {
        ...state,
        todoLists: payload,
      };
    case SHOW_SNACKBAR:
      return {
        ...state,
        showSnackbar: {
          show: true,
          message: payload.message,
          type: payload.type,
        },
      };
    case HIDE_SNACKBAR:
      return {
        ...state,
        showSnackbar: {
          ...state.showSnackbar,
          show: false,
        },
      };
    default:
      return state;
  }
}
