import { createStore } from "redux";
import reducer from "./reducers";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { composeWithDevTools } from "redux-devtools-extension";

const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducer);

// const store = createStore(persistedReducer, composeWithDevTools());

export let store = createStore(persistedReducer, composeWithDevTools());
export let persistor = persistStore(store);
